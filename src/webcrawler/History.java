package webcrawler;

public class History {
			
		private String starttime;
		private String endtime;
		private String lasturl;



		public String getLasturl() {
			return lasturl;
		}

		public void setLasturl(String lasturl) {
			this.lasturl = lasturl;
		}

		public String getStarttime() {
			return starttime;
		}

		public void setStarttime(String starttime) {
			this.starttime = starttime;
		}

		public String getEndtime() {
			return endtime;
		}

		public void setEndtime(String endtime) {
			this.endtime = endtime;
		}
	}

