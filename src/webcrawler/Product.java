package webcrawler;


public class Product {
	private String paytmUrl;
	private String productName;
	private String listPrice;
	private String sellingPrice;
	private String os;
	private String camera;
	private String color;
	private String screen_size;
	private String sim_type;
	private String internal_memory;
	private String ram;
	private String warranty;
	private String brand;
	private String processor;
	private String sim;
	private String primarycamera;
	private String secondarycamera;
	private String batterytype;
	private String batterycapacity;
	private String talktime;
	private String sensors;
	private String importantapps;
	private String otherfeatures;
	private String insalespackage;
	private String returnpolicy;
	private String couponcode;
	private String offer;
	

	public String getCouponcode() {
		return couponcode;
	}

	public void setCouponcode(String couponcode) {
		this.couponcode = couponcode;
	}

	public String getOffer() {
		return offer;
	}

	public void setOffer(String offer) {
		this.offer = offer;
	}

	public String getInsalespackage() {
		return insalespackage;
	}

	public void setInsalespackage(String insalespackage) {
		this.insalespackage = insalespackage;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getListPrice() {
		return listPrice;
	}

	public void setListPrice(String listPrice) {
		this.listPrice = listPrice;
	}

	public String getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(String sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public String getPaytmUrl() {
		return paytmUrl;
	}

	public void setPaytmUrl(String paytmUrl) {
		this.paytmUrl = paytmUrl;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getCamera() {
		return camera;
	}

	public void setCamera(String camera) {
		this.camera = camera;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getScreen_size() {
		return screen_size;
	}

	public void setScreen_size(String screen_size) {
		this.screen_size = screen_size;
	}

	public String getSim_type() {
		return sim_type;
	}

	public void setSim_type(String sim_type) {
		this.sim_type = sim_type;
	}

	public String getInternal_memory() {
		return internal_memory;
	}

	public void setInternal_memory(String internal_memory) {
		this.internal_memory = internal_memory;
	}

	public String getRam() {
		return ram;
	}

	public void setRam(String ram) {
		this.ram = ram;
	}

	public String getWarranty() {
		return warranty;
	}

	public void setWarranty(String warranty) {
		this.warranty = warranty;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getProcessor() {
		return processor;
	}

	public void setProcessor(String processor) {
		this.processor = processor;
	}

	public String getSim() {
		return sim;
	}

	public void setSim(String sim) {
		this.sim = sim;
	}

	public String getPrimarycamera() {
		return primarycamera;
	}

	public void setPrimarycamera(String primarycamera) {
		this.primarycamera = primarycamera;
	}

	public String getSecondarycamera() {
		return secondarycamera;
	}

	public void setSecondarycamera(String secondarycamera) {
		this.secondarycamera = secondarycamera;
	}

	public String getBatterytype() {
		return batterytype;
	}

	public void setBatterytype(String batterytype) {
		this.batterytype = batterytype;
	}

	public String getBatterycapacity() {
		return batterycapacity;
	}

	public void setBatterycapacity(String batterycapacity) {
		this.batterycapacity = batterycapacity;
	}

	public String getTalktime() {
		return talktime;
	}

	public void setTalktime(String talktime) {
		this.talktime = talktime;
	}

	public String getSensors() {
		return sensors;
	}

	public void setSensors(String sensors) {
		this.sensors = sensors;
	}

	public String getImportantapps() {
		return importantapps;
	}

	public void setImportantapps(String importantapps) {
		this.importantapps = importantapps;
	}

	public String getOtherfeatures() {
		return otherfeatures;
	}

	public void setOtherfeatures(String otherfeatures) {
		this.otherfeatures = otherfeatures;
	}

	

	

	public String getReturnpolicy() {
		return returnpolicy;
	}

	public void setReturnpolicy(String returnpolicy) {
		this.returnpolicy = returnpolicy;
	}

	
}
